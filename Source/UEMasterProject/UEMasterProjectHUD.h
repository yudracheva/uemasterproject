// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "UEMasterProjectHUD.generated.h"

UCLASS()
class AUEMasterProjectHUD : public AHUD
{
	GENERATED_BODY()

public:
	AUEMasterProjectHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

