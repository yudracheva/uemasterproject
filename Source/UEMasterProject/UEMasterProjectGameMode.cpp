// Copyright Epic Games, Inc. All Rights Reserved.

#include "UEMasterProjectGameMode.h"
#include "UEMasterProjectHUD.h"
#include "UEMasterProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUEMasterProjectGameMode::AUEMasterProjectGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AUEMasterProjectHUD::StaticClass();
}
