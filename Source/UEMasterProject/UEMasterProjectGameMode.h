// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UEMasterProjectGameMode.generated.h"

UCLASS(minimalapi)
class AUEMasterProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUEMasterProjectGameMode();
};



