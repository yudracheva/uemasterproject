// Copyright Epic Games, Inc. All Rights Reserved.

#include "UEMasterProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UEMasterProject, "UEMasterProject" );
 